package hust.soict.globalict.DateTest;
import java.util.*;

import hust.soict.globalict.DateUtils.DateUtils;
import hust.soict.globalict.DateUtils.StringToNum;
import hust.soict.globalict.MyDate.MyDate;

public class DateTest {
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		System.out.println("User enter a date then reformat it.");
		System.out.println("Enter a date with format as following (February 14th 1888): ");
		String str = keyboard.nextLine();
		MyDate date = new MyDate(str);
		System.out.println("Date after being reformatted to (dd-mm-yyyy): ");
		date.printDate();
		keyboard.close();
		
		System.out.println(" ");
		
		System.out.println("Printing date using new constructor with 3 strings...");
		MyDate md = new MyDate("second", "April", "twenty nineteen");
		String mdStr = StringToNum.monthStr[md.getMonth()-1]+" "+md.getDay()+" "+md.getYear();
		System.out.println(mdStr);
		md.print();
		
		System.out.println(" ");
		
		System.out.println("Sorting several dates and print them");
		MyDate[] mdArr = {new MyDate(11,12,2009), new MyDate(11,12,2015), new MyDate(13,06,2015), new MyDate(04,03,1993)};
		DateUtils du = new DateUtils();
		du.sortDate(mdArr);
		du.printAllDate(mdArr);
		
	}
}
