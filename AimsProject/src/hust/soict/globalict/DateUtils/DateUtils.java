package hust.soict.globalict.DateUtils;

import hust.soict.globalict.MyDate.MyDate;

public class DateUtils {
	public boolean isLater(MyDate md1, MyDate md2) {
		int day1 = md1.getDay();
		int month1 = md1.getMonth();
		int year1 = md1.getYear();
		
		int day2 = md2.getDay();
		int month2 = md2.getMonth();
		int year2 = md2.getYear();
		
		if(year1 > year2) {
			return true;
		}else if(year1 < year2) {
			return false;
		}else {
			if(month1 > month2) {
				return true;
			}else if(month1 < month2) {
				return false;
			}else {
				if(day1 > day2) {
					return true;
				}else if(day1 < day2) {
					return false;
				}else {
					return false;
				}
			}
		}
	}
	
	public void swapDate(MyDate md1, MyDate md2) {
		int dayTmp = md1.getDay();
		int monthTmp = md1.getMonth();
		int yearTmp = md1.getYear();
//		String timeTmp = md1.getTime();
		
		md1.setDay(md2.getDay());
		md1.setMonth(md2.getMonth());
		md1.setYear(md2.getYear());
//		md1.setTime(md2.getTime());
		
		md2.setDay(dayTmp);
		md2.setMonth(monthTmp);
		md2.setYear(yearTmp);
//		md2.setTime(timeTmp);
	}
	
	public void sortDate(MyDate[] mdArr) {
		int n = mdArr.length;
		for(int i=0;i<n;i++) {
			for(int j=i+1;j<n;j++) {
				if(isLater(mdArr[i], mdArr[j])==true) {//Truoc muon hon sau => doi
					swapDate(mdArr[i], mdArr[j]);
				}
			}
		}
	}
	
	public void printAllDate(MyDate[] mdArr) {
		int n = mdArr.length;
		for(int i=0;i<n;i++) {
			mdArr[i].printDate();
		}
	}
	
}
