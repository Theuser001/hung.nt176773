package hust.soict.globalict.DateUtils;

public class StringToNum {
	public static final String monthStr[]= {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
	public static final String dayStr[] = {
			"zero",
			"first",
			"second",
			"third",
			"fourth",
			"fifth",
			"sixth",
			"seventh",
			"eighth",
			"ninth",
			"tenth",
			"eleventh",
			"twelfth",
			"thirteenth",
			"fourteenth",
			"fifteenth",
			"sixteenth",
			"seventeenth",
			"eighteenth",
			"nineteenth",
			"twentyth",
			"twenty-first",
			"twenty-second",
			"twenty-third",
			"twenty-fourth",
			"twenty-fifth",
			"twenty-sixth",
			"twenty-seventh",
			"twenty-eighth",
			"twenty-ninth",
			"thirtieth",
			"thirty-first"
	};
	
	public static final String yearStr[] = {
			"hundred",
			"one",
			"two",
			"three",
			"four",
			"five",
			"six",
			"seven",
			"eight",
			"nine",
			"ten",
			"eleven",
			"twelve",
			"thirteen",
			"fourteen",
			"fifteen",
			"sixteen",
			"seventeen",
			"eighteen",
			"nineteen",
			"twenty",
			"twenty-one",
			"twenty-two",
			"twenty-three",
			"twenty-four",
			"twenty-five",
			"twenty-six",
			"twenty-seven",
			"twenty-eight",
			"twenty-nine",
			"thirty",
			"thirty-one",
			"thirty-two",
			"thirty-three",
			"thirty-four",
			"thirty-five",
			"thirty-six",
			"thirty-seven",
			"thirty-eight",
			"thirty-nine",
			"forty",
			"forty-one",
			"forty-two",
			"forty-three",
			"forty-four",
			"forty-five",
			"forty-six",
			"forty-seven",
			"forty-eight",
			"forty-nine",
			"fifty",
			"fifty-one",
			"fifty-two",
			"fifty-three",
			"fifty-four",
			"fifty-five",
			"fifty-six",
			"fifty-seven",
			"fifty-eight",
			"fifty-nine",
			"sixty",
			"sixty-one",
			"sixty-two",
			"sixty-three",
			"sixty-four",
			"sixty-five",
			"sixty-six",
			"sixty-seven",
			"sixty-eight",
			"sixty-nine",
			"seventy",
			"seventy-one",
			"seventy-two",
			"seventy-three",
			"seventy-four",
			"seventy-five",
			"seventy-six",
			"seventy-seven",
			"seventy-eight",
			"seventy-nine",
			"eighty",
			"eighty-one",
			"eighty-two",
			"eighty-three",
			"eighty-four",
			"eighty-five",
			"eighty-six",
			"eighty-seven",
			"eighty-eight",
			"eighty-nine",
			"ninety",
			"ninety-one",
			"ninety-two",
			"ninety-three",
			"ninety-four",
			"ninety-five",
			"ninety-six",
			"ninety-seven",
			"ninety-eight",
			"ninety-nine"
	};
	
	public int monthToNum(String str) {
		for(int i=0;i<12;i++) {
			if(str.compareTo(monthStr[i])==0) {
				return i+1;
			}
		}
		System.out.println("Invalid month");
		return 0;
	}
	
	public int dayToNum(String str) {
		for(int i=0;i<31;i++) {
			if(str.compareTo(dayStr[i])==0) {
				return i+1;
			}
		}
		System.out.println("Invalid day");
		return 0;
	}
	
	public boolean isInArray(String[] strArr,String str) {
		for(String strElm : strArr) {
			if(str.compareTo(strElm)==0) {
				return true;
			}
		}
		
		return false;
	}
	
	
	
	public int convert(String str) {
		if(str != null) {
			for(int i=0;i<=99;i++) {
				if(str.compareTo(yearStr[i])==0) {
					return i;
				}
			}
		}
		System.out.println("Invalid element of year");
		return 0;
	}

	public int YeartoNum(String str) {
		int a=0,b=0,c=0;
		String[] strTmp = str.split(" ");
		a = convert(strTmp[0]);//Twenty
		if(strTmp.length>=2 && strTmp[1].compareTo("oh")==0) {//Twenty oh
			b = convert(strTmp[2]);
		}else if(strTmp.length>=2 &&strTmp[1].compareTo("thousand")==0) {//Two thousand
			a*=10;
			if(strTmp.length>=3 && strTmp[2].compareTo("and")==0) {//and five
				b = convert(strTmp[3]);
			}else {//five
				b = convert(strTmp[2]);
			}
			
			if(strTmp.length>=4 && strTmp[3].compareTo("hundred")==0) {//nine hundred
				b*=100;
				if(strTmp.length>=5 && strTmp[4].compareTo("and")==0) {//and sixteen
					c = convert(strTmp[5]);
				}
			}
		}else {//fifteen (when strTmp[1] is not either "oh" or "thousand"
			b = convert(strTmp[1]);
			if(a<10 && b<10) {//Loai tru TH two five cho ra kq 205
				a *= 10;
			}
			
			if(b<10) {//Loai tru TH tweny five cho ra kq 2005
				return a+b;
			}
		}
		
		
		return a*100+b+c;//base-case
	}

}
