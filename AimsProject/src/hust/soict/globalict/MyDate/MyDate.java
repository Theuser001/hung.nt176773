package hust.soict.globalict.MyDate;

import java.util.Calendar;

import hust.soict.globalict.DateUtils.StringToNum;

public class MyDate {
	private int day;
	private int month;
	private int year;
//	private String time;
	
	public int getDay() {
		return day;
	}


	public void setDay(int day) {
		this.day = day;
	}


	public int getMonth() {
		return month;
	}


	public void setMonth(int month) {
		this.month = month;
	}


	public int getYear() {
		return year;
	}


	public void setYear(int year) {
		this.year = year;
	}

	

//	public String getTime() {
//		return time;
//	}
//
//
//	public void setTime(String time) {
//		this.time = time;
//	}

	
	public MyDate() {
		super();
		this.day=Calendar.getInstance().get(Calendar.DATE);
		this.month=Calendar.getInstance().get(Calendar.MONTH)+1;
		this.year=Calendar.getInstance().get(Calendar.YEAR);
	}
	
	public MyDate(int day, int month, int year) {
		super();
		this.day = day;
		this.month = month;
		this.year = year;
	}
	
	public MyDate(String time) {//Format: February 14th 1888
		super();
//		this.time = time;
		accept(time);
	}
	
	public MyDate(String day, String month, String year) {
		StringToNum StN = new StringToNum(); //De khong phai dung qua nhieu static trong class StringToNum
		this.day = StN.dayToNum(day);
		this.month = StN.monthToNum(month);
		this.year = StN.YeartoNum(year);
		
	}
	
	
	public boolean dateValidator () {
		if(month==1||month==3||month==5||month==7||month==8||month==10||month==12) {
			if(0<day&&day<32) return true;
			else return false;
		}else if (month==2) {
			if(((year%4==0&&year%100!=0)||year%400==0)) {
				if(0<day&&day<30) return true;
				else return false;
			}else {
				if(0<day&&day<29) return true;
				else return false;
			}
		}else {
			if(0<day&&day<31) return true;
			else return false;
		}
	}
	
	public void isDateInvalid () {
		if(!dateValidator()) {
			System.out.println("Invalid!");
		}else {
//			System.out.println("Valid date!");
		}
	}
	

	public void accept (String date) {//Lay ra day, month, year dang int cua MyDate tu dang String co format February 14th 1888
//		int mark;
		String[] strArr=new String[10];
		String tmp=new String();
		strArr = date.split(" ");
		switch(strArr[0]) {
			case "January":
				this.month=1;
				break;
			case "February":
				this.month=2;
				break;
			case "March":
				this.month=3;
				break;
			case "April":
				this.month=4;
				break;
			case "May":
				this.month=5;
				break;
			case "June":
				this.month=6;
				break;
			case "July":
				this.month=7;
				break;
			case "August":
				this.month=8;
				break;
			case "September":
				this.month=9;
				break;
			case "October":
				this.month=10;
				break;
			case "November":
				this.month=11;
				break;
			case "December":
				this.month=12;
				break;
			default:
				System.out.println("Invalid!");
				this.month=0;
		}
		
		tmp=strArr[1].substring(0,strArr[1].length()-2);
		this.day=Integer.parseInt(tmp);
		
		this.year=Integer.parseInt(strArr[2]);
		
		isDateInvalid();
	}


	public void printDate(){
//		accept(time);
		if(this.month < 10) {
			if(this.day <10) {
				System.out.println("0"+this.day +"-0"+this.month+"-"+this.year);
			}else {
				System.out.println(this.day +"-0"+this.month+"-"+this.year);
			}
		}else {
			if(this.day <10) {
				System.out.println("0"+this.day +"-"+this.month+"-"+this.year);
			}else {
				System.out.println(this.day +"-"+this.month+"-"+this.year);
			}
		}
		
	}
	
	public void print(){
		this.day=Calendar.getInstance().get(Calendar.DATE);
		this.month=Calendar.getInstance().get(Calendar.MONTH)+1;
		this.year=Calendar.getInstance().get(Calendar.YEAR);
		if(this.day==1||this.day==21||this.day==31) {
			System.out.println(this.day+"st "+ StringToNum.monthStr[this.month-1]+" "+this.year);
		}
		else if(this.day==2||this.day==22) {
			System.out.println(this.day+"nd "+ StringToNum.monthStr[this.month-1]+" "+this.year);
		}else if(this.day==3||this.day==13||this.day==23) {
			System.out.println(this.day+"rd "+ StringToNum.monthStr[this.month-1]+" "+this.year);
		}
	}

}
