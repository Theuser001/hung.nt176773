package hust.soict.globalict.Order;
import java.util.ArrayList;
import java.util.Random;

import hust.soict.globalict.MyDate.MyDate;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;

public class Order {
	public static final int MAX_NUMBERS_ORDERED=15;
	public static final int MAX_LIMITED_ORDERS=5;
	private static int nbOrders=0;
	private MyDate dateOrdered=new MyDate(); //Initilized for default as today's date in myDate class
	private ArrayList<Media> itemsOrdered = new ArrayList<Media>();
//	private ArrayList<Media> possibleLuckyItems = new ArrayList<Media>();
	private float totalCost = 0;

	
	public MyDate getDateOrdered() {
		return dateOrdered;
	}

	public void setDateOrdered(MyDate dateOrdered) {
		this.dateOrdered = dateOrdered;
	}
	
	public float getTotalCost() {
		return totalCost;
	}
	
	public ArrayList<Media> getItemsOrdered() {
		return itemsOrdered;
	}

//	public void setItemsOrdered(ArrayList<Media> itemsOrdered) {
//		this.itemsOrdered = itemsOrdered;
//	}
	
	
	public Order () {
		super();
	}
	
	public Order ( ArrayList<Media> mediaList, String orderedTime) {
		super();
		this.itemsOrdered = mediaList;
		this.dateOrdered = new MyDate(orderedTime);
		nbOrders++;
		if(nbOrders > MAX_LIMITED_ORDERS) {
			System.out.println("You have ordered too many times!!!");
		}else {
			System.out.println("You still have "+(MAX_LIMITED_ORDERS-nbOrders)+" oders to go!");
		}
	}
	
	public Order (String orderedTime) {
		super();
		this.dateOrdered = new MyDate(orderedTime);
		nbOrders++;
		if(nbOrders > MAX_LIMITED_ORDERS) {
			System.out.println("You have ordered too many times!!!");
		}else {
			System.out.println("You still have "+(MAX_LIMITED_ORDERS-nbOrders)+" oders to go!");
		}
	}
	
	public void addMedia (Media m) {
		if(itemsOrdered.size() == MAX_NUMBERS_ORDERED) {
			System.out.println("You have reached maximum number of items to order. Can't order more!");
			return;
		}else {
			if(!itemsOrdered.contains(m)) {
				itemsOrdered.add(m);
			}else {
				System.out.println("Item existed in order! Not adding...");
			}
			totalCost += m.getCost();
//			System.out.println("Added!");
		}
	}
	
	public void removeMedia (Media m) {
		if(!itemsOrdered.remove(m)) {
			System.out.println("Can't find your item to remove!");
		}else {
//			System.out.println("Removed!");
		}
	}
	
	public void removeMediaByID (String id) {
		for(int i=0;i<itemsOrdered.size();i++) {
			if(itemsOrdered.get(i).getId()!=null && itemsOrdered.get(i).getId().equals(id)) {
				totalCost -= itemsOrdered.get(i).getCost();
				itemsOrdered.remove(i);
				System.out.println("Removed item with the ID "+id+"!");
				return;
			}
		}
		
		System.out.println("ID not found! Can't remove...");
	}
	
//	public float totalCost() {
//		float total = 0;
//		
//		for(int i=0;i<itemsOrdered.size();i++) {
////			System.out.println(itemsOrdered[i].getCost());
//			total+=itemsOrdered.get(i).getCost();
//		}
//		
//		return total;
//	}
	
	

	public int getALuckyItem(){
		double n= Math.random()*itemsOrdered.size();
		int luckyNumber=(int)n;	
		return luckyNumber;
	}
	
	public int getALuckyItemWithChance(){ 
		float luckyChance = 2/3;
		boolean isLucky = new Random().nextInt((int)(1/(1-luckyChance)))==0; /*https://stackoverflow.com/questions/8183840/probability-in-java*/
		if(isLucky) {
			double n= Math.random()*itemsOrdered.size();
			int luckyNumber=(int)n;	
			return luckyNumber;
		}else {
			return -1;
		}
	}
	
//	public void getALuckyItemEach3Items() {
//		for(int i=0;i<itemsOrdered.size();i++) {
////			System.out.println(itemsOrdered[i].getCost());
//			if(i%3==0) {
//				getALuckyItem();
//			}
//		}
//	}
	
//	public void printAllItems() {
//		for(Media item:itemsOrdered) {
//			item.print();
//		}
//	}
	
	public void printAllItemsAndFinalCost() {
		int luckyNumber=-1;
		int itemNumThreshold = 3;
		int totalCostThreshold = 5;
		float luckyItemCostThresholdRate = 0.1f;
		float luckyItemCostThreshold = totalCost*luckyItemCostThresholdRate;
		boolean isThereItemCostUnderThreshold = false;
		
		
		if(itemsOrdered.size() >= itemNumThreshold && totalCost > totalCostThreshold) {//3 items is the threshold for number of items, 10$ is the threshold for the total cost
			//Duyet arraylist check xem co item nao trong tam gia quy dinh cua lucky item hay khong
			for(int i=0;i<itemsOrdered.size();i++) {
				if(itemsOrdered.get(i).getCost() <= luckyItemCostThreshold) {
					isThereItemCostUnderThreshold = true;
//					possibleLuckyItems.add(itemsOrdered.get(i));
					break;
				}
			}
			
			if(!isThereItemCostUnderThreshold) {
				System.out.println("No items with price under threshold for lucky item's cost!\n");
			}else {
				luckyNumber = getALuckyItem();
				while(itemsOrdered.get(luckyNumber).getCost() > luckyItemCostThreshold) {
					luckyNumber = getALuckyItem();
				}
				System.out.println("Lucky item cost threshold: "+luckyItemCostThreshold);
				System.out.println("Lucky Number: "+luckyNumber+" Lucky item's price: "+itemsOrdered.get(luckyNumber).getCost());
			}
			
			for(int i=0;i<itemsOrdered.size();i++) {
				Media item = itemsOrdered.get(i);
				if(i==luckyNumber) {
					item.print();
					System.out.println("->free");
				}else {
					item.print();
				}
			}
			if(luckyNumber!=-1) {
				System.out.println("Total cost: " + (totalCost-itemsOrdered.get(luckyNumber).getCost()));
			}else {
				System.out.println("Total cost: " + (totalCost));
			}
			
		}else {
			for(int i=0;i<itemsOrdered.size();i++) {
				Media item = itemsOrdered.get(i);
				if(i==luckyNumber) {
					item.print();
					System.out.println("->free");
				}else {
					item.print();
				}
			}
			System.out.println("Total cost: " + totalCost);
		}
		
		
	}


}
