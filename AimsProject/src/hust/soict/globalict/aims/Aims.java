package hust.soict.globalict.aims;

import hust.soict.globalict.MyDate.MyDate;
import hust.soict.globalict.Order.Order;
import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.media.Track;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class Aims {
	
	public static void showMenu() {
//		System.out.println("\tI'm so bored I do this...");
		System.out.println("\t ---------------------------------------------------------");
		System.out.println("\t: O R D E R   M A N A G E M E N T   A P P L I C A T I O N :");
		System.out.println("\t:---------------------------------------------------------:");
		System.out.println("\t: 1. Create new order                                     :");
		System.out.println("\t: 2. Add item to the order                                :");
		System.out.println("\t: 3. Delete item by id                                    :");
		System.out.println("\t: 4. Display the items list of order                      :");
		System.out.println("\t: 0. Exit                                                 :");
		System.out.println("\t ---------------------------------------------------------");
		System.out.println("\t\t     Please choose a number: 0-1-2-3-4");
		System.out.print("\t\t\t      >>Option: ");
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
//		MemoryDaemon md = new MemoryDaemon();
//		Thread thr = new Thread(md);
//		thr.setDaemon(true);
//		thr.start();
		
		Scanner keyboard = new Scanner(System.in);
		String opt = "Nothing";
		Order o=null;
		
		do{
			try {
				//			thr.run();
				showMenu();
				opt = keyboard.nextLine();
//				System.out.println("Opt: "+opt);
				switch(opt) {
					case("1"):
						System.out.println("Creating new order...Disposing old orders...");
	//					System.out.println("What's the date of the order?(Example: July 27th 2019): ");
	//					String str = keyboard.nextLine();
	//					o = new Order(str);
						o = new Order();
						System.out.println("Order created!");
			
						break;
					case("2"):
						System.out.println("Adding a new item to order...");
						if(o==null) {
							System.out.println("You have to create an order first!");
						}else {
	//						System.out.println(o);
							System.out.print("What type of media is it?\n  1.CD\n  2.Book\n  3.Digital Video Disc\n  >>Your choice: ");
							String type = keyboard.nextLine();
							switch(type) {
								case("1"):
									System.out.print("Id: ");
									String idCD = keyboard.nextLine();
									System.out.print("Title: ");
									String titCD = keyboard.nextLine();
									System.out.print("Category: ");
									String catCD = keyboard.nextLine();
									System.out.print("Cost: ");
									float costCD = keyboard.nextFloat();
									keyboard.nextLine();
									System.out.print("Director: ");
									String dirCD = keyboard.nextLine();
									System.out.print("Artist: ");
									String artCD = keyboard.nextLine();
									ArrayList<Track> tracksCD = new ArrayList<Track>();
									CompactDisc cd = new CompactDisc(idCD, titCD, catCD, costCD, dirCD, artCD, tracksCD);
									System.out.print("Add track?(Y/n): ");
									String isAddTrack = keyboard.nextLine();
									while(!isAddTrack.equals("N") && !isAddTrack.equals("n")) {
	//									System.out.print(!isAddTrack.equals("N") && !isAddTrack.equals("n"));
										System.out.print("Track's title: ");
										String titTrack = keyboard.nextLine();
										System.out.print("Track's length: ");
										int lenTrack = keyboard.nextInt();
										keyboard.nextLine();
										Track newTrack = new Track(titTrack, lenTrack);
										cd.addTrack(newTrack);
										System.out.print("Add more track?(Y/n): ");
										isAddTrack=keyboard.nextLine();
									}
									cd.setMediaType("CD");
									o.addMedia(cd);
									System.out.println("Added!");
									System.out.print("Play CDs?(Y/n): ");
									String isPlayCD = keyboard.nextLine();
									if(!isPlayCD.equals("N") && !isPlayCD.equals("n") ) {
										cd.play();
									}else {
										System.out.println("Currently not playing any track...");
									}
									
									break;
								case("2"):
									System.out.print("Id: ");
									String idBook = keyboard.nextLine();
									System.out.print("Title: ");
									String titBook = keyboard.nextLine();
									System.out.print("Category: ");
									String catBook = keyboard.nextLine();
									System.out.print("Cost: ");
									float costBook = keyboard.nextFloat();
									keyboard.nextLine();
									System.out.print("Authors(separate by ','): ");
									String authBookTmp=keyboard.nextLine();
									String[] authBookTmpArr = authBookTmp.split(",");
									List<String> authBook = new ArrayList<String>();
									for(String author:authBookTmpArr) {
										authBook.add(author);
									}
									Book book = new Book(idBook,titBook, catBook, costBook, authBook);
									book.setMediaType("Book");
									o.addMedia(book);
									System.out.println("Added!");
									break;
								case("3"):
									System.out.print("Id: ");
									String idDVD = keyboard.nextLine();
									System.out.print("Title: ");
									String titDVD = keyboard.nextLine();
									System.out.print("Category: ");
									String catDVD = keyboard.nextLine();
									System.out.print("Cost: ");
									float costDVD = keyboard.nextFloat();
									keyboard.nextLine();
									System.out.print("Director: ");
									String dirDVD = keyboard.nextLine();
									System.out.print("Length: ");
									int lenDVD = keyboard.nextInt();
									keyboard.nextLine();
									DigitalVideoDisc dvd = new DigitalVideoDisc(idDVD,titDVD,catDVD,costDVD,dirDVD,lenDVD);
									dvd.setMediaType("DVD");
									o.addMedia(dvd);
									System.out.println("Added!");
									break;
								default:
									System.out.println("Wrong choice!");
							}
						}
						break;
					case("3"):
						if(o==null) {
							System.out.println("You have to create an order first!");
						}else {
							System.out.println("Deleting item by id...");
							System.out.print("Input item's id: ");
							String id=keyboard.nextLine();
							o.removeMediaByID(id);
						}
						break;
					case("4"):
						if(o==null) {
							System.out.println("You have to create an order first!");
						}else {
							System.out.println("Dislaying items list of order...");
							System.out.print(" Date of Order: ");
							o.getDateOrdered().printDate();
							o.printAllItemsAndFinalCost();
						}
						break;
					case("0"):
						System.out.println("\t\t     Exiting program...\n\t\t     Exited!");
						break;
					default:
						System.out.println("Wrong option! Try again.");
				}
			}catch(InputMismatchException e) {
				System.out.println("Exception: " + e.getMessage() + ". Make sure all the cost and length fields are numbers and of appropriate form (float for cost fields, int for length fields)");
			}catch(PlayerException e) {
				System.out.println("Exception: " + e.getMessage());
			}catch(ArithmeticException e) {
				System.out.println("Exception: " + e.getMessage());
			}catch(ClassCastException e) {
				System.out.println("Exception: " + e.getMessage());
			}
			catch(Exception e) {
				System.out.println("Exception: " + e.getMessage());
				e.printStackTrace();
			}
			finally {
				System.out.println("Finished catching exceptions!");
			}
			
		}while(!opt.equals("0"));
		keyboard.close();
	}
}
