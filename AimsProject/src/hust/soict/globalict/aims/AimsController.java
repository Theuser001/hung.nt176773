package hust.soict.globalict.aims;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.media.Track;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import hust.soict.globalict.aims.Data;

 

public class AimsController implements Initializable {
	
	@FXML
	MenuItem addCDMenu;
	@FXML
	MenuItem addBookMenu;
	@FXML
	MenuItem addDVDMenu;
	@FXML
	TableView<Media> itemTable;
	@FXML
	TableColumn<Media, String> idCol;
	@FXML
	TableColumn<Media, String> titCol;
	@FXML
	TableColumn<Media, String> catCol; 
	@FXML
	TableColumn<Media, Float> costCol;
	@FXML
	MenuButton addItemBtn;
	@FXML
	MenuButton delItemBtn;
	@FXML
	Button checkOutBtn;
	@FXML
	TextField deletedIDField;
	@FXML
	Button delButton;

	
	static ObservableList<Media> oListItems = FXCollections.observableArrayList(Data.order.getItemsOrdered());
	
	
	public AimsController() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub
		System.out.println("Check Table display");
		idCol.setCellValueFactory(new PropertyValueFactory<>("id"));
		titCol.setCellValueFactory(new PropertyValueFactory<>("title"));
		catCol.setCellValueFactory(new PropertyValueFactory<>("category"));
		costCol.setCellValueFactory(new PropertyValueFactory<>("cost"));
		
//		Media m1 = new Media("ujewoew", "wueojd", "jjo", 1.12f) {
//		};
//		Media m2 = new Media("ujewoew", "wueojd", "jjo", 1.2f) {
//		};
		
		
//		oListItems.add(m1);
//		oListItems.add(m2);
		itemTable.setItems(oListItems);
		
	}
	
	public void switchToAddCDScreen(ActionEvent event) {
		try {
//			Data.tracksCD = new ArrayList<Track>(); //Reset lai Data.tracksCD neu luc truoc bam Add track ma khong bam Add CD
			
			Data.cd = new CompactDisc(); //Reset Data.cd de add CD moi
			
			System.out.println("Adding CD...");
			Stage addCDScreen = new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("addCDView.fxml"));
			addCDScreen.setTitle("Add CD");
			addCDScreen.setScene(new Scene(root));
			
			addCDScreen.initModality(Modality.APPLICATION_MODAL);
			addCDScreen.show();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void switchToAddBookScreen(ActionEvent event) {
		try {
			System.out.println("Adding Book...");
			Stage addBookScreen = new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("addBookView.fxml"));
			addBookScreen.setTitle("Add Book");
			addBookScreen.setScene(new Scene(root));
			
			addBookScreen.initModality(Modality.APPLICATION_MODAL);
			addBookScreen.show();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void switchToAddDVDScreen(ActionEvent event) {
		try {
			System.out.println("Adding DVD...");
			Stage addDVDScreen = new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("addDVDView.fxml"));
			addDVDScreen.setTitle("Add DVD");
			addDVDScreen.setScene(new Scene(root));
			
			addDVDScreen.initModality(Modality.APPLICATION_MODAL);
			addDVDScreen.show();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public int getALuckyItem(){
		double n= Math.random()*oListItems.size();
		int luckyNumber=(int)n;	
		return luckyNumber;
	}
	
	public void checkOut (ActionEvent event) {
		
		if(oListItems.size() > 1 && Data.luckyNumber == -1) {
			Data.luckyNumber=getALuckyItem();
		}
		
		for(int i=0;i<oListItems.size();i++) {
			Media item = oListItems.get(i);
			if(i==Data.luckyNumber) {
				item.print();
				System.out.println("->free");
			}else {
				//item.print();
				Data.totalCost += item.getCost();
			}
		}
		
		Notifications.showCheckOutAlert();
	}
	
	public void deleteByID (ActionEvent event) {
		String deletedID = deletedIDField.getText();
		
		for(int i=0;i<oListItems.size();i++) {
			Media item = oListItems.get(i);
			
			if(item.getId().compareTo(deletedID)==0) {
				oListItems.remove(item);
			}else {
				System.out.println("No item with such id!");
				Notifications.showIDNotFoundAlert();
			}
		}
		
		deletedIDField.clear();
	}

}
