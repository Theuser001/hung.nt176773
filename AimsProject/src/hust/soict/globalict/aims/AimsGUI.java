package hust.soict.globalict.aims;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import hust.soict.globalict.Order.Order;
import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.media.Track;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class AimsGUI extends Application{

	public AimsGUI() {
		// TODO Auto-generated constructor stub
	}
	
	public void start(Stage primaryStage) {
		try {
			 Parent root = FXMLLoader.load(getClass().getResource("NewOrderView.fxml"));
			 primaryStage.setTitle("Aims Java");
			 primaryStage.setScene(new Scene(root));
			 primaryStage.show();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
