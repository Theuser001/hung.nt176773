package hust.soict.globalict.aims;

public class MemoryDaemon implements Runnable{
	private long memoryUsed = 0;
	
	public MemoryDaemon () {
		// TODO Auto-generated constructor stub
	}
	
	public void run() {
		Runtime rt = Runtime.getRuntime();
		long used;
		while (true) {
			used = rt.totalMemory()-rt.freeMemory();
			if(used != memoryUsed) {
				System.out.println("Detect memory changes...");
				System.out.println("\tTotal memory = "+rt.totalMemory()+"\n\tFree memory = "+rt.freeMemory());
				System.out.println("\tMemory used = "+used);
				memoryUsed = used;
				System.out.println("\n");
			}
		}
	}
	
}
