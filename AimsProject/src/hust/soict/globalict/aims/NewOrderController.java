package hust.soict.globalict.aims;

import java.net.URL;
import java.util.ResourceBundle;
import hust.soict.globalict.Order.Order;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class NewOrderController implements Initializable {

	private static int nbOrders = 0;

	@FXML
	private Button newOrderBtn;
	@FXML
	private Button exitBtn;

	public NewOrderController() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub
	}

	public void switchToMainScreen(ActionEvent event) {
		// Reset cac gia tri o Data va AimsController.oListItems ve nhu ban dau (Khoi
		// tao mot order moi voi cac gia trimac dinh)
		Data.order = new Order(); // Khoi tao mot order moi
		AimsController.oListItems.clear(); // Xoa het cac item trong itemList cua lan order truoc
		Data.totalCost = 0;
		Data.luckyNumber = -1;

		try {
//			System.out.println("Creating a new order...");
			nbOrders++;
			if (nbOrders > Order.MAX_LIMITED_ORDERS) {
				Notifications.showTooMuchOrdersAlert();
				return;
			}
			Stage mainScreen = new Stage();
			Parent root = FXMLLoader.load(getClass().getResource("AimsView.fxml"));
			mainScreen.setTitle("Order #" + nbOrders);
			mainScreen.setScene(new Scene(root));

			mainScreen.initModality(Modality.APPLICATION_MODAL);
			mainScreen.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void exitProgram(ActionEvent event) {
		System.out.println("Exiting...");
		System.exit(0);
	}

}
