package hust.soict.globalict.aims;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import hust.soict.globalict.Order.Order;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class Notifications {

	public Notifications() {
		// TODO Auto-generated constructor stub
	}

	public static void showTooMuchOrdersAlert() {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Notification");

		// Header Text: null
		alert.setHeaderText(null);
		alert.setContentText("Maximum number of order reached!");

		alert.showAndWait();
	}

	public static void showDuplicateTrackAlert() {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Notification");

		// Header Text: null
		alert.setHeaderText(null);
		alert.setContentText("Duplicated track! Try again.");

		alert.showAndWait();
	}

	public static void showDuplicateMediaAlert() {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Notification");

		// Header Text: null
		alert.setHeaderText(null);
		alert.setContentText("Duplicated item's ID. Try again.");

		alert.showAndWait();
	}

	public static void showMaxItemsOrderedAlert() {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Notification");

		// Header Text: null
		alert.setHeaderText(null);
		alert.setContentText("You have reached maximum number of items in an order: " + Order.MAX_NUMBERS_ORDERED
				+ " Can't order more!");

		alert.showAndWait();
	}

	public static void showCheckOutAlert() {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Notification");

		// Header Text: null
		alert.setHeaderText(null);

		String timeStamp = new SimpleDateFormat("dd-MM-yyyy").format(Calendar.getInstance().getTime());
		if (Data.luckyNumber > -1) {
			alert.setContentText("Total money: " + Data.totalCost + "$!\n Item with ID: "
					+ AimsController.oListItems.get(Data.luckyNumber).getId() + " Title: "
					+ AimsController.oListItems.get(Data.luckyNumber).getTitle() + " Category: "
					+ AimsController.oListItems.get(Data.luckyNumber).getCategory() + " Cost: "
					+ AimsController.oListItems.get(Data.luckyNumber).getCost() + " is discounted.\nDate: "
					+ timeStamp);
		} else {
			alert.setContentText("Total money: " + Data.totalCost + "$!\nDate: " + timeStamp);
		}

		Data.totalCost = 0;
		alert.showAndWait();
	}
	
	public static void showIDNotFoundAlert() {
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Notification");

		// Header Text: null
		alert.setHeaderText(null);
		alert.setContentText("There's no item with such ID in the order!");

		alert.showAndWait();
	}
	
	public static void showNoEmptyFieldAlert() {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Notification");

		// Header Text: null
		alert.setHeaderText(null);
		alert.setContentText("You have to fill all the form");

		alert.showAndWait();
	}
	
	public static void showWrongCostFieldFormatAlert() {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Notification");

		// Header Text: null
		alert.setHeaderText(null);
		alert.setContentText("Cost must be of float value and >= 0.");

		alert.showAndWait();
	}
	
	public static void showWrongDVDCostAndLengthFieldsFormatAlert() {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Notification");

		// Header Text: null
		alert.setHeaderText(null);
		alert.setContentText("Cost must be of float value and >= 0 & length must be of int value and > 0.");

		alert.showAndWait();
	}
	
	public static void showWrongLengthFieldFormatAlert() {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Notification");

		// Header Text: null
		alert.setHeaderText(null);
		alert.setContentText("Length must be of int value and > 0.");

		alert.showAndWait();
	}
}
