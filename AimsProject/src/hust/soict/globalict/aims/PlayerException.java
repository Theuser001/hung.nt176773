package hust.soict.globalict.aims;

public class PlayerException extends Exception {

	public PlayerException (String msg) {
		super(msg);
	}
}
