package hust.soict.globalict.aims;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import hust.soict.globalict.Order.Order;
import hust.soict.globalict.aims.media.Book;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class addBookController implements Initializable {

	@FXML
	private Button addBookBtn;
	@FXML
	private TextField idField;
	@FXML
	private TextField titField;
	@FXML
	private TextField catField;
	@FXML
	private TextField cosField;
	@FXML
	private TextField autField;

	public addBookController() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub

	}

	public void addBook(ActionEvent event) {
		try {
			String idBook = idField.getText();
			String titBook = titField.getText();
			String catBook = catField.getText();
			float costBook = 0;
			if (cosField.getText() != null && cosField.getText().length() > 0) {
				costBook = Float.parseFloat(cosField.getText());
			} else {
				costBook = 0;
			}
			
			String authBookTmp = autField.getText();
			
			String[] authBookTmpArr = authBookTmp.split(",");
			List<String> authBook = new ArrayList<String>();
			for(String author:authBookTmpArr) {
				authBook.add(author);
			}
			
			Book book = new Book(idBook,titBook,catBook,costBook,authBook);
			
//			book.print();
			
			if (AimsController.oListItems.size() == Order.MAX_NUMBERS_ORDERED) {
				Notifications.showMaxItemsOrderedAlert();
				return;
			} else {
				if (!AimsController.oListItems.contains(book)) {
					AimsController.oListItems.add(book); // DUNG RA NEN TAO MOT OBSERVABLE LIST O Data.java CHU KHONG PHAI
														// TAO O AIM CONTROLLER CHO CO TINH THONG NHAT LA MOI GIA TRI
														// DEU DUOC LAY TU STAGE NAY TRUYEN QUA Data.java ROI TRUYEN QUA
														// STAGE KHAC ??
				} else {
					Notifications.showDuplicateMediaAlert();
					System.out.println("Item existed in order! Not adding...");
				}
				
				idField.clear();
				titField.clear();
				catField.clear();
				cosField.clear();
				autField.clear();
				
				//Close window after clicking the Add Book Button
//				Stage stage = (Stage) addBookBtn.getScene().getWindow();
//			    stage.close();
			}
		} catch (NumberFormatException e) { 
			Notifications.showWrongCostFieldFormatAlert();
		}catch (ArithmeticException e) {
			Notifications.showWrongCostFieldFormatAlert();
		}catch (Exception e) {
			// TODO: handle exception
//			e.printStackTrace();
			System.out.println("Exception found: " + e);
		}
	}

}
