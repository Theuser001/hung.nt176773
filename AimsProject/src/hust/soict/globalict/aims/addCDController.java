package hust.soict.globalict.aims;
//import hust.soict.globalict.aims.AimsController;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;
import hust.soict.globalict.Order.Order;
import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.Track;
import hust.soict.globalict.aims.Data;

public class addCDController implements Initializable {

	@FXML
	private Button addTrackBtn;
	@FXML
	private Button addCDBtn;
	@FXML
	private TextField idField;
	@FXML
	private TextField titField;
	@FXML
	private TextField catField;
	@FXML
	private TextField cosField;
	@FXML
	private TextField dirField;
	@FXML
	private TextField artField;
	@FXML
	private TextField trackTitField;
	@FXML
	private TextField trackLenField;
	@FXML
	private Pane addCDPane;
	@FXML
	private Pane addTrackPane;
	@FXML
	TableView<Track> trackTable;
	@FXML
	TableColumn<Track, String> trackTitCol;
	@FXML
	TableColumn<Track, Integer> trackLenCol;

	public addCDController() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		// TODO Auto-generated method stub

	}

	public void addCD(ActionEvent event) {
		try {

			String idCD = idField.getText();
			String titCD = titField.getText();
			String catCD = catField.getText();
			float costCD = 0;
			if (cosField.getText() != null && cosField.getText().length() > 0) {
				costCD = Float.parseFloat(cosField.getText());
			} else {
				costCD = 0;
			}
			String dirCD = dirField.getText();
			String artCD = artField.getText();
			CompactDisc cd = new CompactDisc(idCD, titCD, catCD, costCD, dirCD, artCD, Data.cd.getTracks()); // Luc nay
																												// tracks
																												// chua
																												// co gi

			// Add CD to AimsController.oListItems
			if (AimsController.oListItems.size() == Order.MAX_NUMBERS_ORDERED) {
				Notifications.showMaxItemsOrderedAlert();
				return;
			} else {
				if (!AimsController.oListItems.contains(cd)) {
					AimsController.oListItems.add(cd); // DUNG RA NEN TAO MOT OBSERVABLE LIST O Data.java CHU KHONG PHAI
														// TAO O AIM CONTROLLER CHO CO TINH THONG NHAT LA MOI GIA TRI
														// DEU DUOC LAY TU STAGE NAY TRUYEN QUA Data.java ROI TRUYEN QUA
														// STAGE KHAC ??

					addTrackPane.setDisable(false);
					addCDPane.setDisable(true); // Neu add CD roi moi cho add track thi phai disable addCD sau khi add
												// xong CD
				} else {
					Notifications.showDuplicateMediaAlert();
					System.out.println("Item existed in order! Not adding...");
				}
			}

		}catch (NumberFormatException e) { 
			Notifications.showWrongCostFieldFormatAlert();
		}catch (ArithmeticException e) {
			Notifications.showWrongCostFieldFormatAlert();
		}catch (Exception e) {
			// TODO: handle exception
//			e.printStackTrace();
			System.out.println("Exception found: " + e);
		}
	}

	public void addTrack(ActionEvent event) {
		try {

			String titTrack = trackTitField.getText();
			int lenTrack = 0;
			if (trackLenField.getText() != null && trackLenField.getText().length() > 0) {
				lenTrack = Integer.parseInt(trackLenField.getText());
			} else {
				lenTrack = 0;
			}
			Track newTrack = new Track(titTrack, lenTrack);
//			Data.tracksCD.add(newTrack); //->Neu dung Data.tracksCD.add(newTrack)

			if (!Data.cd.getTracks().contains(newTrack)) {// check xem track them vao co bi trung hay khong
				Data.cd.getTracks().add(newTrack);
			} else {
				Notifications.showDuplicateTrackAlert();
			}

			System.out.println(Data.cd.getTracks().size() + "track(s)!");

			trackTitField.clear();
			trackLenField.clear();
		} catch (NumberFormatException e) { 
			Notifications.showWrongLengthFieldFormatAlert();
		}catch (ArithmeticException e) {
			Notifications.showWrongLengthFieldFormatAlert();
		}catch (Exception e) {
			// TODO: handle exception
//			e.printStackTrace();
			System.out.println("Exception found: " + e);
		}
	}

	public void playTrack(ActionEvent event) {
		trackTitCol.setCellValueFactory(new PropertyValueFactory<>("title"));
		trackLenCol.setCellValueFactory(new PropertyValueFactory<>("length"));

		ObservableList<Track> oListTrack = FXCollections.observableArrayList(Data.cd.getTracks());
		trackTable.setItems(oListTrack);
	}

}
