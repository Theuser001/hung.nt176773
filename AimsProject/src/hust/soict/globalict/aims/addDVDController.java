package hust.soict.globalict.aims;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import hust.soict.globalict.Order.Order;
import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class addDVDController implements Initializable {

	@FXML
	private Button addDVDBtn;
	@FXML
	private TextField idField;
	@FXML
	private TextField titField;
	@FXML
	private TextField catField;
	@FXML
	private TextField cosField;
	@FXML
	private TextField dirField;
	@FXML
	private TextField lenField;

	public addDVDController() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		// TODO Auto-generated method stub

	}

	public void addDVD(ActionEvent event) {
		try {
			String idDVD = idField.getText();
			String titDVD = titField.getText();
			String catDVD = catField.getText();
			float costDVD = 0;
			if (cosField.getText() != null && cosField.getText().length() > 0) {
				costDVD = Float.parseFloat(cosField.getText());
			} else {
				costDVD = 0;
			}
			
			String dirDVD = dirField.getText();
			int lenDVD = 0;
			if (lenField.getText() != null && lenField.getText().length() > 0) {
				lenDVD = Integer.parseInt(lenField.getText());
			} else {
				lenDVD = 0;
			}
			DigitalVideoDisc dvd = new DigitalVideoDisc(idDVD, titDVD, catDVD, costDVD, dirDVD, lenDVD);
			
//			book.print();
			
			if (AimsController.oListItems.size() == Order.MAX_NUMBERS_ORDERED) {
				Notifications.showMaxItemsOrderedAlert();
				return;
			} else {
				if (!AimsController.oListItems.contains(dvd)) {
					AimsController.oListItems.add(dvd); // DUNG RA NEN TAO MOT OBSERVABLE LIST O Data.java CHU KHONG PHAI
														// TAO O AIM CONTROLLER CHO CO TINH THONG NHAT LA MOI GIA TRI
														// DEU DUOC LAY TU STAGE NAY TRUYEN QUA Data.java ROI TRUYEN QUA
														// STAGE KHAC ??
				} else {
					Notifications.showDuplicateMediaAlert();
					System.out.println("Item existed in order! Not adding...");
				}
				
				idField.clear();
				titField.clear();
				catField.clear();
				cosField.clear();
				dirField.clear();
				lenField.clear();
				
				//Close window after clicking the Add Book Button
//				Stage stage = (Stage) addBookBtn.getScene().getWindow();
//			    stage.close();
			}
		} catch (NumberFormatException e) { 
			Notifications.showWrongDVDCostAndLengthFieldsFormatAlert();
		}catch (ArithmeticException e) {
			Notifications.showWrongDVDCostAndLengthFieldsFormatAlert();
		}catch (Exception e) {
			// TODO: handle exception
//			e.printStackTrace();
			System.out.println("Exception found: " + e);
		}
	}

}

