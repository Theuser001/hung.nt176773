package hust.soict.globalict.aims.media;
import java.util.*;

public class Book extends Media{
	
	private List<String> authors = new ArrayList<String>();
	private String content;
	private List<String> contentTokens = new ArrayList<String>();
	private Map<String, Integer> wordFrequency = new TreeMap<String, Integer>();

	public Book() {
		// TODO Auto-generated constructor stub
	}
	
	public Book(String title) {
		super(title);
	}
	
	public Book (String title, String category) {
		super(title, category);
	}
	
	public Book(String title, String category, List<String> authors) {
		super(title, category);
		this.authors = authors;
	}
	
	public Book(String id, String title, String category, float cost, List<String> authors) throws InputMismatchException, ArithmeticException {
		super(id, title, category, cost);
		this.authors = authors;
	}
	
	public Book(String id, String title, String category, float cost, List<String> authors, String content) throws InputMismatchException, ArithmeticException{
		this(id, title, category, cost, authors);
		this.content = content;
	}
	
	public List<String> getAuthors() {
		return authors;
	}
	
	public void addAuthor (String author) {
		for(int i=0;i<authors.size();i++) {
			if(authors.get(i).equals(author)) {
				System.out.println("Author has already existed, not adding!");
				return;
			}
			authors.add(author);
//			System.out.println("Added!");
		}
	}
	
	public void removeAuthor (String author) {
		for(int i=0;i<authors.size();i++) {
			if(authors.get(i).equals(author)) {
				authors.remove(author);
				System.out.println("Removed!");
				return;
			}
		}
		System.out.println("Author does not exist, not deleting!");
		return;
	}
	
	public void print () {
		System.out.println("Id: "+this.getId()+/*" - Type: "+this.getMediaType()+*/" - Title: "+this.getTitle()+" - Category: "+this.getCategory()+" - Authors: "+this.authors+" - Cost: "+this.getCost());
	}
	
	
	public void processContent() {
//		Scanner keyboard = new Scanner(System.in);
//		System.out.println("Input content: ");
//		content = keyboard.nextLine();
	
		String[] tempContentTokens = content.split(" |\\.|\\,");
		Arrays.sort(tempContentTokens);
		for(String tempToken:tempContentTokens) {
			contentTokens.add(tempToken);
//			System.out.println(tempToken);
		}
		
		for(String token:contentTokens) {
			wordFrequency.put(token, Collections.frequency(contentTokens, token));
		}
		
//		System.out.println(wordFrequency);
		
	}

	public String toString() {
		return ("Id: "+this.getId()+/*" - Type: "+this.getMediaType()+*/" - Title: "+this.getTitle()+" - Category: "+this.getCategory()+" - Authors: "+this.authors+" - Cost: "+this.getCost()+" - Number of tokens: "+contentTokens.size()+" - Token list and frequency: "+wordFrequency);
	}
}
