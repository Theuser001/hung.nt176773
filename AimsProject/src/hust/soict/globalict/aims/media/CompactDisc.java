package hust.soict.globalict.aims.media;

import java.util.ArrayList;
import java.util.InputMismatchException;

import hust.soict.globalict.aims.PlayerException;

public class CompactDisc extends Disc implements Playable {
	
	private String artist;
//	private int length;
	private ArrayList<Track> tracks = new ArrayList<Track>();

	public CompactDisc() {
		// TODO Auto-generated constructor stub
	}
	
	public CompactDisc(String id, String title, String category, float cost, String director, String artist, ArrayList<Track> tracks) throws InputMismatchException {
		super(id, title, category, cost, director);
		this.artist = artist;
		this.tracks = tracks;
		
	}
 	public String getArtist() {
		return artist;
	}
	
	public ArrayList<Track> getTracks() {
		return tracks;
	}
	
	public boolean isTrackExisted(Track track) {
		for(int i=0;i<this.tracks.size();i++) {
			if(this.tracks.get(i).equals(track)) {
				return true;
			}
		}
		
		return false;
	}
	
	public void addTrack(Track track) {
//		System.out.println(tracks.contains(track));
		if(!tracks.contains(track)) {
			this.tracks.add(track);
		}else {
			System.out.println("Track existed, not adding...");
		}
		
	}
	
	public void removeTrack(Track track) {
		if(!isTrackExisted(track)) {
			System.out.println("Track does not exist, couldn't delete...");
		}else {
			this.tracks.remove(track);
		}
	}
	
	public int getLength () {
		for(int i=0;i<this.tracks.size();i++) {
			length += this.tracks.get(i).getLength();
		}
		
		return length;
	}
	
	public void play() throws PlayerException {
//		System.out.println("Playing CD: " + this.getTitle());
//		System.out.println("CD's total length: " + this.getLength());
//		
//		for(int i=0;i<this.tracks.size();i++) {
//			this.tracks.get(i).play();
//		}
		if(this.getLength() > 0) {
			System.out.println("Playing CD: " + this.getTitle());
			System.out.println("CD's total length: " + this.getLength());
			
			for(int i=0;i<this.tracks.size();i++) {
				this.tracks.get(i).play();
			}
			
			java.util.Iterator iter = tracks.iterator();
			Track nextTrack;
			while(iter.hasNext()) {
				nextTrack = (Track) iter.next();
				try {
					nextTrack.play();
				}catch(PlayerException e) {
					throw e;
				}
			}
		}else {
			throw new PlayerException("CD length is non-positive");
		}
	}
	
	public int compareTo(Object o) {
		CompactDisc m = (CompactDisc) o;
		if(tracks.size()>m.tracks.size()) {
			return 1;
		}else if(tracks.size() == m.tracks.size()) {
			if(this.length > m.getLength()) {
				return 1;
			}else if(this.getLength() == m.getLength()) {
				return 0;
			}else {
				return -1;
			}
		}else {
			return -1;
		}
		
	}
	
}
