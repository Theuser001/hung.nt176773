package hust.soict.globalict.aims.media;

import java.util.InputMismatchException;

import hust.soict.globalict.aims.PlayerException;

public class DigitalVideoDisc extends Disc implements Playable{
	private String director;
	
	public DigitalVideoDisc(String id, String title, String category, float cost,String director, int length) throws InputMismatchException {
		super(id, title, category, cost);
		this.director = director;
		this.length = length;
		if (length <= 0) {
			throw new ArithmeticException("Length of DVD must be > 0.");
		}
	}
	
	public DigitalVideoDisc(String title) {
		super(title);
	}
	
	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}
	
	//method which finds out (case insensitive) if the corresponding disk of the current object contains all the tokens(separted by " ") of the string str.
//	public boolean search(String str) {
//		String titleTmp = this.title.toLowerCase();
//		String strTmp = str.toLowerCase();
//		String[] strArr = strTmp.split(" ");
//		for(String token : strArr){
//			if(!titleTmp.contains(token)) {
//				return false;
//			}
//		}
//		return true;
//	}
//	
	public void print () {
		System.out.println("Id: "+this.getId()+/*" - Type: "+this.getMediaType()+*/" - Title: "+this.getTitle()+" - Category: "+this.getCategory()+" - Director: "+this.director+" - Length: "+this.length+" - Cost: "+this.getCost());
	}
	
	public void play() throws PlayerException{
		if(this.getLength() > 0) {
			System.out.println("Playing DVD: " + this.getTitle());
			System.out.println("DVD length: " + this.getLength());
		}else {
			throw new PlayerException("DVD length is non-positive");
		}
	}
	
	public int compareTo(Object o) {
		DigitalVideoDisc m = (DigitalVideoDisc) o;
		if(this.getCost()>m.getCost()) {
			return 1;
		}else if(this.getCost()==m.getCost()) {		
			return 0;
		}else {
			return -1;
		}
	}
}
