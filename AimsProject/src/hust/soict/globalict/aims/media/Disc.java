package hust.soict.globalict.aims.media;

import java.util.InputMismatchException;

public class Disc extends Media {
	private String director;
	protected int length;
	
	public Disc() {
		// TODO Auto-generated constructor stub
	}
	
	public Disc(String director) {
		super();
		this.director = director;
	}

	public Disc(int length) {
		super();
		this.length = length;
	}
	
	public Disc(String director, int length) {
		this.director = director;
		this.length = length;
	}
		
	public Disc(String id, String title, String category, float cost) throws InputMismatchException {
		super(id, title, category, cost);
	}
	
	public Disc(String id, String title, String category, float cost, String director) throws InputMismatchException {
		super(id, title, category, cost);
		this.director = director;
	}
	
	public Disc(String id, String title, String category, float cost, String director, int length) throws InputMismatchException {
		super(id, title, category, cost);
		this.director = director;
		this.length = length;
	}

	public String getDirector() {
		return director;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public int getLength() {
		return length;
	}
	public void setLength(int length) {
		this.length = length;
	}

}
