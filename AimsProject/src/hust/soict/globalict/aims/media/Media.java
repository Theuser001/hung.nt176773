package hust.soict.globalict.aims.media;

import java.util.InputMismatchException;

public abstract class Media/*<T>*/ implements Comparable/*<T>*/ {
	private String id;
	private String title;
	private String category;
	private float cost;
	
	protected String mediaType; //Media, Book, DVD, CD

	public String getMediaType() {
		return mediaType;
	}

	public void setMediaType(String mediaType) {
		this.mediaType = mediaType;
	}

	public Media() {
		// TODO Auto-generated constructor stub
		
	}
	
	public Media (String title) {
		this.title = title;
	}
	
	public Media (String title, String category) {
		this(title);
		this.category = category;
	}
	
	public Media (String title, String category, float cost) throws InputMismatchException, ArithmeticException {
		this(title,category);
		this.cost = cost;
		if(cost < 0) {
			throw new ArithmeticException("Cost must be >= 0.");
		}
	}
	
	public Media (String id, String title, String category, float cost) throws InputMismatchException,ArithmeticException {
		this(title,category,cost);
		this.id = id;
		if(cost < 0) {
			throw new ArithmeticException("Cost must be >= 0.");
		}
	}

	
	public String getTitle() {
		return title;
	}


//	public void setTitle(String title) {
//		this.title = title;
//	}


	public String getCategory() {
		return category;
	}


//	public void setCategory(String category) {
//		this.category = category;
//	}


	public float getCost() {
		return cost;
	}


//	public void setCost(float cost) {
//		this.cost = cost;
//	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
	public void print () {
		System.out.println("Id: "+this.id+/*" - Type: "+this.mediaType+*/" - Title: "+this.title+" - Category: "+this.category+" - Cost: "+this.cost);
	}
	
	public boolean equals(Object o) { 
		try {
			if(o instanceof Media) {//Avoid Exceptions/Errors
				Media m = (Media) o;
				return id.equals(m.getId());
			}else {
				throw new ClassCastException("Object is not an instace of Media!"); //unchecked (runtime) exception -> no need to use 'throws' ?
			}
		} catch (NullPointerException e) {
			// TODO: handle exception
			System.out.println("Exception:"+e.getMessage());
		}
		
		return false;
	}
	
//	public int compareTo(T o) {
//		Media m = (Media) o;
//		if(title.compareTo(m.getTitle())>0) {
//			return 1;
//		}else if(title.compareTo(m.getTitle())==0) {		
//			return 0;
//		}else {
//			return -1;
//		}
//	}
	
	public int compareTo(Object o) {
		Media m = (Media) o;
		if(title.compareTo(m.getTitle())>0) {
			return 1;
		}else if(title.compareTo(m.getTitle())==0) {		
			return 0;
		}else {
			return -1;
		}
	}
}
