package hust.soict.globalict.aims.media;

import java.util.*;

public class TestMediaCompareTo {

	public TestMediaCompareTo() {
		// TODO Auto-generated constructor stub
	}
	
	
	
	public static void main(String[] args) {
		try {
			List<DigitalVideoDisc> collection  = new ArrayList<DigitalVideoDisc>(); //Up cast
			List <CompactDisc> collection1  = new ArrayList<CompactDisc>(); //Up cast
			DigitalVideoDisc dvd1 = new DigitalVideoDisc("01","The Overloader","Sci-fi",9.92f,"J.J ABraham", 120);
			DigitalVideoDisc dvd2 = new DigitalVideoDisc("02","King of midst","Fantasy",7.45f,"Lenoel Lenoyle",95);
			DigitalVideoDisc dvd3 = new DigitalVideoDisc("03","Judge the fallen might!","Historical",8.48f,"Kingsley Rodr", 132);
			
			Track track1 = new Track("tr1", 15);
			Track track2 = new Track("tr2", 23);
			Track track3 = new Track("tr3", 47);
			Track track4 = new Track("tr4", 10);
			Track track5 = new Track("tr5",19);
			Track track6 = new Track("tr6",27);
			
			ArrayList<Track> springList = new ArrayList<Track>();
			ArrayList<Track> summerList= new ArrayList<Track>();
			ArrayList<Track> autumnList = new ArrayList<Track>();
			
			springList.add(track1);
			springList.add(track2);
			summerList.add(track3);
			autumnList.add(track4);
			autumnList.add(track5);
			autumnList.add(track6);
	;		
			
			CompactDisc cd1 = new CompactDisc("04", "Spring Time", "Audiophile", 1.99f, "Livc", "Mach", springList);
			CompactDisc cd2 = new CompactDisc("05", "Summer Time", "Audiophile", 1.99f, "Yanc", "Mach", summerList);
			CompactDisc cd3 = new CompactDisc("06", "Autumn TIme", "Audiophile", 1.99f, "Mach", "Mach", autumnList);
			
			collection1.add(cd1);
			collection1.add(cd2);
			collection1.add(cd3);
			
			collection.add(dvd1);
			collection.add(dvd2);
			collection.add(dvd3);
			
			Iterator iterator = collection.iterator();
			Iterator iterator1 = collection1.iterator();
			
			System.out.println("----------------------------------------------");
			System.out.println("The DVDs currently in the order are: ");
			System.out.println("..");
			while (iterator.hasNext()) {
//				((DigitalVideoDisc)iterator.next()).play();
				((DigitalVideoDisc)iterator.next()).print();
				System.out.println("------");
			}
			
			Collections.sort((List)collection);
			iterator = collection.iterator();
			
			System.out.println("----------------------------------------------");
			System.out.println("The DVDs in sorted order are: ");
			System.out.println("..");
			while (iterator.hasNext()) {
//				((DigitalVideoDisc)iterator.next()).play();
				((DigitalVideoDisc)iterator.next()).print();
				System.out.println("------");
			}
			
			
			System.out.println("----------------------------------------------");
			
			
			System.out.println("----------------------------------------------");
			System.out.println("The CDs currently in the order are: ");
			System.out.println("..");
			while (iterator1.hasNext()) {
//				((DigitalVideoDisc)iterator.next()).play();
				((CompactDisc)iterator1.next()).play();
				System.out.println("------");
			}
			
			Collections.sort((List)collection1);
			iterator1 = collection1.iterator();
			
			System.out.println("----------------------------------------------");
			System.out.println("The CDs in sorted order are: ");
			System.out.println("..");
			while (iterator1.hasNext()) {
//				((DigitalVideoDisc)iterator.next()).play();
				((CompactDisc)iterator1.next()).play();
				System.out.println("------");
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	

}
