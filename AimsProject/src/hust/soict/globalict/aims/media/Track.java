package hust.soict.globalict.aims.media;

import java.util.InputMismatchException;

import hust.soict.globalict.aims.PlayerException;

public class Track/*<T>*/ implements Playable, Comparable/*<T>*/{
	private String title;
	private int length;
	
	public Track() {
		// TODO Auto-generated constructor stub
	}

	public Track(String title, int length) throws InputMismatchException/*, ArithmeticException*/ {
		super();
		this.title = title;
		this.length = length;
//		if(length <= 0) {
//			throw new ArithmeticException("Length of track must be > 0.");
//		}
	}

	public String getTitle() {
		return title;
	}
	
	public int getLength() {
		return length;
	}
	
	public boolean areTracksEqual(Track track) {
		boolean areTracksEqual = false;
		if(this.title == track.getTitle() && this.length == track.getLength()) {
			areTracksEqual = true;
		}
		
		return areTracksEqual;
	}
	
	public boolean equals(Object o) {
//		if(o instanceof Media) {
			Track tr = (Track) o;
			return (title.equals(tr.title)&&(length==tr.length));
//		}else {
//			return false;
//		}
		
	}
	
	public void play() throws PlayerException{
		if(this.getLength() > 0) {
			System.out.println("Playing track: " + this.getTitle());
			System.out.println("Track length: " + this.getLength());
		}else {
			throw new PlayerException("Track length is non-positive");
		}
	}

	@Override
//	public int compareTo(T o) {
//		// TODO Auto-generated method stub
//		Track<?> t = (Track<?>) o;
//		if(title.compareTo(t.getTitle())>0) {
//			return 1;
//		}else if(title.compareTo(t.getTitle())==0) {		
//			return 0;
//		}else {
//			return -1;
//		}
//	}
	
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		Track t = (Track) o;
		if(title.compareTo(t.getTitle())>0) {
			return 1;
		}else if(title.compareTo(t.getTitle())==0) {		
			return 0;
		}else {
			return -1;
		}
	}

}
