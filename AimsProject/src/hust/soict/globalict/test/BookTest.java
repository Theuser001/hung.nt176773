package hust.soict.globalict.test;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import hust.soict.globalict.Order.Order;
import hust.soict.globalict.aims.media.Book;
import hust.soict.globalict.aims.media.CompactDisc;
import hust.soict.globalict.aims.media.DigitalVideoDisc;
import hust.soict.globalict.aims.media.Media;
import hust.soict.globalict.aims.media.Track;

public class BookTest {

	public BookTest() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			Scanner keyboard = new Scanner(System.in);
			String opt = "Nothing";
			Order o=null;
			System.out.print("Add book?(Y/n): ");
			opt = keyboard.nextLine();
			while(!opt.equals("N") && !opt.equals("n")){
				System.out.print("Id: ");
				String idBook = keyboard.nextLine();
				System.out.print("Title: ");
				String titBook = keyboard.nextLine();
				System.out.print("Category: ");
				String catBook = keyboard.nextLine();
				System.out.print("Cost: ");
				float costBook = keyboard.nextFloat();
				keyboard.nextLine();
				System.out.print("Authors(separate by ','): ");
				String authBookTmp=keyboard.nextLine();
				String[] authBookTmpArr = authBookTmp.split(",");
				List<String> authBook = new ArrayList<String>();
				for(String author:authBookTmpArr) {
					authBook.add(author);
				}
				System.out.println("Input content: ");
				String contBook = keyboard.nextLine();
				Book book = new Book(idBook,titBook, catBook, costBook, authBook,contBook);
				book.processContent();
				System.out.println("Added: "+book.toString());
				System.out.print("Add another book?(Y/n): ");
				opt = keyboard.nextLine();
			}
			keyboard.close();
			System.out.println("Done!");
		}catch (Exception e) {
//			e.printStackTrace();
			System.out.println("BookTest is having error...");
		}
	}

}
