package hust.soict.globalict.gui.awt;
import java.awt.*;
import java.awt.event.*;

public class BtnCountListener implements ActionListener {
	AWTCounterNamedOrdinaryOuterClass frame;
   public BtnCountListener(AWTCounterNamedOrdinaryOuterClass frame) {
      this.frame = frame;
   }
   
   @Override
   public void actionPerformed(ActionEvent evt) {
      frame.count++;
      frame.tfCount.setText(frame.count + "");
   }
}
