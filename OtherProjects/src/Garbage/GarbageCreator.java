package Garbage;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class GarbageCreator {
	public static void main(String[] args) {
		
		String data = "", dataTmp = "";
		int garbageCnt = 0;
		// TODO Auto-generated method stub
		try {
//			File f0 = new File("testNormal.TXT");
			File f0 = new File("testGarbage.TXT");
			Scanner myReader = new Scanner(f0);
			while (myReader.hasNextLine()) {
//				dataTmp = data;
				data += myReader.nextLine();
//				System.gc();
				if(!data.equals(dataTmp)) {
					garbageCnt++;
				}
				if(garbageCnt > 1000) {
					System.out.println("Too much garbage(>1000)! Terminating programm...\nTerminated!");
					System.exit(0);
				}
				
//				System.out.println(data);
			}
			myReader.close();
		}catch(FileNotFoundException e) {
			System.out.println("Error reading file!");
			e.printStackTrace();
		}
		
		System.out.println(data);
	}

}
