package Garbage;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class NoGarbage {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		StringBuffer data = new StringBuffer();
		// TODO Auto-generated method stub
		try {
//			File f0 = new File("testNormal.TXT");
			File f0 = new File("testGarbage.TXT");
			Scanner myReader = new Scanner(f0);
			while (myReader.hasNextLine()) {
				data.append(myReader.nextLine());
//				System.out.println(data);
			}
			myReader.close();
		}catch(FileNotFoundException e) {
			System.out.println("Error reading file!");
			e.printStackTrace();
		}
		
		System.out.println(data);
	}

}
